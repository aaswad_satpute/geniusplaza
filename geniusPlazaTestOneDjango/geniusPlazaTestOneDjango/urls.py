"""geniusPlazaTestOneDjango URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from geniusPlazaTestOneApp import views

urlpatterns = [
    path('', views.index),
    path('create_recipe/<str:recipe_name>', views.create_recipe),
    path('create_user/<str:username>', views.create_user),
    path('get_recipe_by_user/<str:username>', views.get_recipe_by_user),
    path('get_all_recipes', views.get_all_recipes),
    path('update_recipe/<str:recipe_name>', views.update_recipe),
    path('delete_recipe/<str:recipe_name>', views.delete_recipe)
]
