from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from geniusPlazaTestOneApp.models import (Step, User, Ingredient, Recipe)
import json


def get_all_recipes(request):
    if request.method == 'GET':
        try:
            recipe_list = []
            for recipe_object in Recipe.objects.all():
                recipe = {}
                recipe['name'] = recipe_object.name
                recipe['user'] = f'{recipe_object.user.first_name} {recipe_object.user.last_name}'
                recipe_steps = []
                recipe_ingredient = []
                for step_object in Step.objects.filter(recipe=recipe_object).all():
                    recipe_steps.append(str(step_object))
                for ingredient_object in Ingredient.objects.filter(recipe=recipe_object).all():
                    recipe_ingredient.append(str(ingredient_object))
                recipe['steps'] = recipe_steps
                recipe['ingredients'] = recipe_ingredient
                recipe_list.append(recipe)
            response = json.dumps(recipe_list)
        # TODO: Make this non-generic. I agree this is not the best exception handling.
        except Exception as e:
            print(e)
            response = json.dumps({'Error': 'No recipes found.'})
    else:
        response = json.dumps(
            {'Error': 'This function supports just a GET method.'})
    return HttpResponse(response, content_type='text/json')


def index(request):
    if request.method == 'GET':
        response = json.dumps({'Message': 'Welcome to the Recipe Book!'})
    else:
        response = json.dumps(
            {'Error': 'This function supports just a GET method.'})
    return HttpResponse(response, content_type='text/json')


@csrf_exempt
def create_user(request, username):
    if request.method == 'POST':
        try:
            email = request.POST.get('email', None)
            first_name = request.POST.get('first_name', None)
            last_name = request.POST.get('last_name', None)
            password = request.POST.get('password', None)
            user = User(username=username, email=email, first_name=first_name,
                        last_name=last_name, password=password)
            user.save()
            response = json.dumps(
                {'Message': f'User {username}, was sucessfully created!'})
        # TODO: Make this non-generic. I agree this is not the best exception handling.
        except Exception as e:
            response = json.dumps(
                {'Error': f'Sorry! The user {username}, already exists.'})
    else:
        response = json.dumps(
            {'Error': 'This function supports just a POST method.'})
    return HttpResponse(response, content_type='text/json')


@csrf_exempt
def create_recipe(request, recipe_name):
    if request.method == 'POST':
        try:
            username = request.POST.get('username', None)
            steps = request.POST.get('steps', None).split(", ")
            ingredients = request.POST.get('ingredients', None).split(", ")
            found_user = User.objects.get(username=username)
            recipe = Recipe(name=recipe_name, user=found_user)
            Step.objects.filter(recipe=recipe).delete()
            Ingredient.objects.filter(recipe=recipe).delete()
            recipe.save()
            for step in steps:
                Step(step_text=step, recipe=recipe).save()
            for ingredient in ingredients:
                Ingredient(ingredient_text=ingredient, recipe=recipe).save()
            response = json.dumps(
                {'Message': f'Recipe {recipe_name}, by user {username}, was sucessfully created!'})
        # TODO: Make this non-generic. I agree this is not the best exception handling.
        except Exception as e:
            response = json.dumps(
                {'Error': f'Sorry! Could not create the recipe.'})
    else:
        response = json.dumps(
            {'Error': 'This function supports just a POST method.'})
    return HttpResponse(response, content_type='text/json')


def get_recipe_by_user(request, username):
    if request.method == 'GET':
        try:
            recipe_list = []
            found_user = User.objects.get(username=username)
            for recipe_object in Recipe.objects.filter(user=found_user).all():
                recipe = {}
                recipe['name'] = recipe_object.name
                recipe['user'] = f'{recipe_object.user.first_name} {recipe_object.user.last_name}'
                recipe_steps = []
                recipe_ingredient = []
                for step_object in Step.objects.filter(recipe=recipe_object).all():
                    recipe_steps.append(str(step_object))
                for ingredient_object in Ingredient.objects.filter(recipe=recipe_object).all():
                    recipe_ingredient.append(str(ingredient_object))
                recipe['steps'] = recipe_steps
                recipe['ingredients'] = recipe_ingredient
                recipe_list.append(recipe)
            response = json.dumps(recipe_list)
        # TODO: Make this non-generic. I agree this is not the best exception handling.
        except Exception as e:
            response = json.dumps(
                {'Error': f'No recipes found by the user, {username}'})
    else:
        response = json.dumps(
            {'Error': 'This function supports just a GET method.'})
    return HttpResponse(response, content_type='text/json')


@csrf_exempt
def delete_recipe(request, recipe_name):
    if request.method == 'DELETE':
        try:
            response = json.dumps(
                Recipe.objects.filter(name=recipe_name).delete())
        # TODO: Make this non-generic. I agree this is not the best exception handling.
        except Exception as e:
            response = json.dumps(
                {'Error': f'Could not delete the recipe, {recipe_name}'})
    else:
        response = json.dumps(
            {'Error': 'This function supports just a DELETE method.'})
    return HttpResponse(response, content_type='text/json')


@csrf_exempt
def update_recipe(request, recipe_name):
    if request.method == 'POST':
        try:
            steps = request.POST.get('steps', None).split(", ")
            ingredients = request.POST.get('ingredients', None).split(", ")
            recipe = Recipe.objects.get(name=recipe_name)
            Step.objects.filter(recipe=recipe).delete()
            Ingredient.objects.filter(recipe=recipe).delete()
            for step in steps:
                Step(step_text=step, recipe=recipe).save()
            for ingredient in ingredients:
                Ingredient(ingredient_text=ingredient, recipe=recipe).save()
            response = json.dumps(
                {'Message': f'Recipe {recipe_name}, was sucessfully updated!'})
        # TODO: Make this non-generic. I agree this is not the best exception handling.
        except Exception as e:
            print(e)
            response = json.dumps(
                {'Error': f'Sorry! Could not update the recipe.'})
    else:
        response = json.dumps(
            {'Error': 'This function supports just a POST method.'})
    return HttpResponse(response, content_type='text/json')
