from django.apps import AppConfig


class GeniusplazatestoneappConfig(AppConfig):
    name = 'geniusPlazaTestOneApp'
